<?php
/**
 * Template Name: Page with Sidebar
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package capc
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main container-fluid" role="main">
				<div class="row">
					<div class="col-sm-9 capc-page-content">
						<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', 'page' );
						endwhile; // End of the loop.
						?>
					</div>
					<div class="col-sm-3 capc-success-stories-sidebar capc-page-sidebar">
						<?php
						$quotes_query = new WP_Query(array(
							'post_type' => 'page',
							'name' => 'quotes', //Quotes
							'posts_per_page' => 1
						));
						if($quotes_query->have_posts()) {
							?>
							<h3>Quotes</h3>
							<?php
							while($quotes_query->have_posts()) {
								$quotes_query->the_post();
								$quotes = explode(PHP_EOL,get_the_content());
								echo $quotes[array_rand($quotes)];
							}
						}
						$success_stories_query = new WP_Query(array(
							'post_type' => 'page',
							'post_parent' => '66', //Success stories
							'posts_per_page' => 2,
							'orderby' => 'rand'
						));
						if($success_stories_query->have_posts()) {
							?>
							<h3>Success Stories</h3>
							<?php
							while ($success_stories_query->have_posts()) {
								$success_stories_query->the_post();
								echo get_the_post_thumbnail() . '<h4><a href="' . get_permalink() . '">' . get_the_title() . '</a></h4><p>' . get_the_excerpt() . '</p><a href="' . get_permalink() . '">Read More &raquo;</a><br><br>';
							}
						}
						?>
					</div>
				</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
