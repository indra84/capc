<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package capc
 */

?>

	</div><!-- #content -->
	<hr>
	<footer id="colophon" class="site-footer capc-footer" role="contentinfo">
		<?php
			$menu_items = wp_get_nav_menu_items( 'navbar', array() );
			$menu_html = "<ul class='capc-footer-links'>";
			foreach($menu_items as $menu_item) {
				if($menu_item->menu_item_parent==0) {
					$menu_html .= "<li><a href='" . $menu_item->url . "'>" . $menu_item->title . "</a></li>";
				}
			}
			$menu_html .= "</ul>";
			echo $menu_html;
		?>
		<div class="text-center capc-footer-social-links">
			<a href="https://www.facebook.com/CAPC.COCO" target="_blank" title="CAPC COCO Facebook page"><img src="<?php bloginfo('template_url'); ?>/img/facebook.png" /></a>&emsp;
			<a href="https://twitter.com/PreventKidAbuse" target="_blank" title="CAPC COCO Twitter page"><img src="<?php bloginfo('template_url'); ?>/img/twitter.png" /></a>&emsp;
			<a href="https://visitor.r20.constantcontact.com/manage/optin?v=001OSVZXasZuMoCbDXCHBjdlftnctdBcuRE" target="_blank" title="CAPC COCO Newletter"><img src="<?php bloginfo('template_url'); ?>/img/email.png" /></a>
		</div>
		<br>
		<div><strong>Child Abuse Prevention Council of Contra Costa County</strong></div>
		<div class="capc-address">2120 Diamond Blvd., #120 | Concord, CA 94520 | (925) 798-0546</div>
		<div class="text-muted">Copyright &copy; <?php echo date("Y") ?></div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</div>
<p class="text-center hidden-xs capc-footer-help"><span class="glyphicon glyphicon-info-sign"></span> To adjust font-size on a computer:<br> Hold down <strong>ctrl</strong> (PC) or <strong>cmd</strong> (Mac) on your keyboard. Press <span class="glyphicon glyphicon-plus"></span> to make the text larger or <span class="glyphicon glyphicon-minus"></span> to make the text smaller</p>
</body>
</html>
