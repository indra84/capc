<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package capc
 */
if ($redirecturls[$wp->request]) {
	wp_redirect($redirecturls[$wp->request], 301);
	exit();
}
if($wp->request=="search" && !empty($_GET['q'])) {
	wp_redirect(home_url('/search/' . urlencode($_GET['q'])));
	exit();
}

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main container-fluid" role="main">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">Oops! That page can't be found</h1>
				</header><!-- .page-header -->
				<div class="page-content">
					<p>It looks like nothing was found at this location. Maybe try one of the links below</p>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
