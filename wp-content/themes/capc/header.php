<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package capc
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NLRP9G"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLRP9G');</script>
<!-- End Google Tag Manager -->
<div class="page-wrapper">
<!--Menu-->
<nav class="navbar navbar-default navbar-capc">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><img alt="CAPC-COCO" src="<?php bloginfo('template_url'); ?>/img/logo.jpg"></a>
		</div>
		<div class="collapse navbar-collapse" id="menu">
			<div class="capc-top-bar">
				<form action="/search" id="search-form">
					<div class="input-group">
						<input type="text" name="q" placeholder="Search" class="form-control" id="search-input" value="<?php get_search_query();?>">
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit" id="search-button"><span class="glyphicon glyphicon-search"></span></button>
						</span>
					</div>
				</form>
				<form id="translate-form">
					<select id="translate-select" class="form-control">
						<option value="">Google Translate</option>
						<option value="en">English</option>
						<option value="es">Spanish</option>
						<option value="vi">Vietnamese</option>
						<option value="zh-CN">Chinese (Simplified)</option>
					</select>
				</form>
			</div>
			<ul class="nav navbar-nav">
				<?php
					$menu_items = wp_get_nav_menu_items( 'navbar', array() );
					$menu_html = "";
					$sub_menu_html = "";
					foreach($menu_items as $menu_item) {
						if($menu_item->menu_item_parent==0) {
							if(strlen($sub_menu_html)>0) {
								$menu_html .= $sub_menu_html . "</ul>";
								$sub_menu_html = "";
							} else if(strlen($menu_html)>0) {
								$menu_html .= "</li>";
							}
							$menu_html .= "<li><a href='" . $menu_item->url . "'>" . $menu_item->title . "</a>";
						} else {
							if(strlen($sub_menu_html)==0) {
								$sub_menu_html = "<ul class='capc-sub-menu'>";
							}
							$sub_menu_html .= "<li><a href='" . $menu_item->url . "'>" . $menu_item->title . "</a></li>";
						}
					}
					if(strlen($sub_menu_html)>0) {
						$menu_html .= $sub_menu_html . "</ul>";
					}
					echo $menu_html;
				?>
			</ul>
		</div>
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('capc-header-widget') ) : ?>
		<?php endif; ?>
		<div class="navbar-tagline">&ldquo;<?php bloginfo('description'); ?>&rdquo;</div>
		<div class="clearfix visible-xs"></div>
		<?php dynamic_sidebar( 'header' ); ?>
	</div>
</nav>
<div id="page" class="site capc-site">
	<div id="content" class="site-content">
