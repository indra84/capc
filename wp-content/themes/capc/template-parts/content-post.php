<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package capc
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' );?>
		<p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php the_date(); ?>&emsp;<span class="glyphicon glyphicon-tags"></span> <?php the_category(', '); ?></p>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php

			if(has_post_thumbnail()) {
				?>
				<img class="capc-image" src="<?php the_post_thumbnail_url(); ?>" />
				<?php
			} else {
				$media = get_field('media');
				if($media) {
					if(strpos($media,"youtube")) {
						echo '<div class="capc-video">' . $media . '</div>';
					} else {
						echo $media;
					}
				}
			}
			echo '<br>';
			the_content();
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<br>
<div id="disqus_thread"></div>
<script>
var disqus_config = function () {
this.page.url = '<?php echo get_permalink(); ?>';
this.page.identifier = '<?php echo $post->post_name . '/' . $post->ID; ?>';
};
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = '//capc-coco.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<br>
