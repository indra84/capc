<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package capc
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if($post->post_name != 'home') { ?>
			<?php
			$title = get_field('page_title');
			if(!$title) {
				if(tribe_is_month() || tribe_is_upcoming() || tribe_is_past() || tribe_is_day()) {
					$title = tribe_get_events_title();
				} else {
					$title = get_the_title($post);
				}
			}
			if($title) { ?>
				<header class="page-header">
					<h1 class="entry-title"><?php echo $title ?></h1>
					<?php if($post->post_parent) {
						echo "<a href='" . get_permalink($post->post_parent) . "'>" . get_the_title($post->post_parent) . "</a> &raquo; " . $post->post_title;
					} ?>
				</header><!-- .entry-header -->
				<?php
			}
	}?>
	<div class="entry-content">
		<?php
			if(has_post_thumbnail()) {
				?>
				<img class="capc-image capc-image-right" src="<?php the_post_thumbnail_url(); ?>" />
				<?php
			}
			the_content();
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
